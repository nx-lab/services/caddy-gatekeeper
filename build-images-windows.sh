#!/bin/bash

set -e

docker build -t watheialabs/caddy-gatkeeper:ci-nanoserver-1803 -f Dockerfile-nanoserver-1803 .
docker build -t watheialabs/caddy-gatkeeper:ci-nanoserver-1809 -f Dockerfile-nanoserver-1809 .
