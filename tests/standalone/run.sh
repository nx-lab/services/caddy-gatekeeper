#!/bin/bash

set -e

. ../functions.sh

docker stack deploy -c compose.yaml --prune caddy_test

retry curl --show-error -s -k -f --resolve whoami0.watheia.io:443:127.0.0.1 https://whoami0.watheia.io &&
retry curl --show-error -s -k -f --resolve whoami1.watheia.io:443:127.0.0.1 https://whoami1.watheia.io &&
retry curl --show-error -s -k -f --resolve whoami2.watheia.io:443:127.0.0.1 https://whoami2.watheia.io &&
retry curl --show-error -s -k -f --resolve whoami3.watheia.io:443:127.0.0.1 https://whoami3.watheia.io &&
retry curl --show-error -s -k -f --resolve echo0.watheia.io:443:127.0.0.1 https://echo0.watheia.io/sourcepath/something || {
    docker service logs caddy_test_caddy
    exit 1
}
