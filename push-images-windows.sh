#!/bin/bash

set -e

if [[ "${BUILD_SOURCEBRANCH}" == "refs/heads/main" ]]; then
    echo "Pushing CI images"
    docker login -u lucaslorentz -p "$DOCKER_PASSWORD"
    docker push watheialabs/caddy-gatkeeper:ci-nanoserver-1803
    docker push watheialabs/caddy-gatkeeper:ci-nanoserver-1809
fi

if [[ "${RELEASE_VERSION}" =~ ^v[0-9]+\.[0-9]+\.[0-9]+(-.*)?$ ]]; then
    echo "Releasing version ${RELEASE_VERSION}..."

    PATCH_VERSION=$(echo $RELEASE_VERSION | cut -c2-)
    MINOR_VERSION=$(echo $PATCH_VERSION | cut -d. -f-2)

    docker login -u lucaslorentz -p "$DOCKER_PASSWORD"

    # nanoserver-1803
    docker tag watheialabs/caddy-gatkeeper:ci-nanoserver-1803 watheialabs/caddy-gatkeeper:nanoserver-1803
    docker tag watheialabs/caddy-gatkeeper:ci-nanoserver-1803 watheialabs/caddy-gatkeeper:${PATCH_VERSION}-nanoserver-1803
    docker tag watheialabs/caddy-gatkeeper:ci-nanoserver-1803 watheialabs/caddy-gatkeeper:${MINOR_VERSION}-nanoserver-1803
    docker push watheialabs/caddy-gatkeeper:nanoserver-1803
    docker push watheialabs/caddy-gatkeeper:${PATCH_VERSION}-nanoserver-1803
    docker push watheialabs/caddy-gatkeeper:${MINOR_VERSION}-nanoserver-1803

    # nanoserver-1809
    docker tag watheialabs/caddy-gatkeeper:ci-nanoserver-1809 watheialabs/caddy-gatkeeper:nanoserver-1809
    docker tag watheialabs/caddy-gatkeeper:ci-nanoserver-1809 watheialabs/caddy-gatkeeper:${PATCH_VERSION}-nanoserver-1809
    docker tag watheialabs/caddy-gatkeeper:ci-nanoserver-1809 watheialabs/caddy-gatkeeper:${MINOR_VERSION}-nanoserver-1809
    docker push watheialabs/caddy-gatkeeper:nanoserver-1809
    docker push watheialabs/caddy-gatkeeper:${PATCH_VERSION}-nanoserver-1809
    docker push watheialabs/caddy-gatkeeper:${MINOR_VERSION}-nanoserver-1809
fi
