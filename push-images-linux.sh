#!/bin/bash

set -e

if [[ "${BUILD_SOURCEBRANCH}" == "refs/heads/main" ]]; then
    echo "Pushing CI images"
    
    docker login -u watheialabs -p "$DOCKER_PASS"

    docker push watheialabs/caddy-gatkeeper:ci
    docker push watheialabs/caddy-gatkeeper:ci-alpine
    docker push watheialabs/caddy-gatkeeper:ci-arm32v6
    docker push watheialabs/caddy-gatkeeper:ci-alpine-arm32v6
fi

if [[ "${RELEASE_VERSION}" =~ ^v[0-9]+\.[0-9]+\.[0-9]+(-.*)?$ ]]; then
    echo "Releasing version ${RELEASE_VERSION}..."

    PATCH_VERSION=$(echo $RELEASE_VERSION | cut -c2-)
    MINOR_VERSION=$(echo $PATCH_VERSION | cut -d. -f-2)

    docker login -u watheialabs -p "$DOCKER_PASS"

    # scratch
    docker tag watheialabs/caddy-gatkeeper:ci watheialabs/caddy-gatkeeper:latest
    docker tag watheialabs/caddy-gatkeeper:ci watheialabs/caddy-gatkeeper:${PATCH_VERSION}
    docker tag watheialabs/caddy-gatkeeper:ci watheialabs/caddy-gatkeeper:${MINOR_VERSION}
    docker push watheialabs/caddy-gatkeeper:latest
    docker push watheialabs/caddy-gatkeeper:${PATCH_VERSION}
    docker push watheialabs/caddy-gatkeeper:${MINOR_VERSION}

    # alpine
    docker tag watheialabs/caddy-gatkeeper:ci-alpine watheialabs/caddy-gatkeeper:alpine
    docker tag watheialabs/caddy-gatkeeper:ci-alpine watheialabs/caddy-gatkeeper:${PATCH_VERSION}-alpine
    docker tag watheialabs/caddy-gatkeeper:ci-alpine watheialabs/caddy-gatkeeper:${MINOR_VERSION}-alpine
    docker push watheialabs/caddy-gatkeeper:alpine
    docker push watheialabs/caddy-gatkeeper:${PATCH_VERSION}-alpine
    docker push watheialabs/caddy-gatkeeper:${MINOR_VERSION}-alpine

    # scratch arm32v6
    docker tag watheialabs/caddy-gatkeeper:ci-arm32v6 watheialabs/caddy-gatkeeper:latest-arm32v6
    docker tag watheialabs/caddy-gatkeeper:ci-arm32v6 watheialabs/caddy-gatkeeper:${PATCH_VERSION}-arm32v6
    docker tag watheialabs/caddy-gatkeeper:ci-arm32v6 watheialabs/caddy-gatkeeper:${MINOR_VERSION}-arm32v6
    docker push watheialabs/caddy-gatkeeper:latest-arm32v6
    docker push watheialabs/caddy-gatkeeper:${PATCH_VERSION}-arm32v6
    docker push watheialabs/caddy-gatkeeper:${MINOR_VERSION}-arm32v6

    # alpine arm32v6
    docker tag watheialabs/caddy-gatkeeper:ci-alpine-arm32v6 watheialabs/caddy-gatkeeper:alpine-arm32v6
    docker tag watheialabs/caddy-gatkeeper:ci-alpine-arm32v6 watheialabs/caddy-gatkeeper:${PATCH_VERSION}-alpine-arm32v6
    docker tag watheialabs/caddy-gatkeeper:ci-alpine-arm32v6 watheialabs/caddy-gatkeeper:${MINOR_VERSION}-alpine-arm32v6
    docker push watheialabs/caddy-gatkeeper:alpine-arm32v6
    docker push watheialabs/caddy-gatkeeper:${PATCH_VERSION}-alpine-arm32v6
    docker push watheialabs/caddy-gatkeeper:${MINOR_VERSION}-alpine-arm32v6
fi