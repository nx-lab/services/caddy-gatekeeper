#!/bin/bash

set -e

chmod +x artifacts/binaries/linux/amd64/caddy
docker build -t watheialabs/caddy-gatkeeper:ci -f Dockerfile .
docker build -t watheialabs/caddy-gatkeeper:ci-alpine -f Dockerfile-alpine .

chmod +x artifacts/binaries/linux/arm32v6/caddy
docker build -t watheialabs/caddy-gatkeeper:ci-arm32v6 -f Dockerfile-arm32v6 .
docker build -t watheialabs/caddy-gatkeeper:ci-alpine-arm32v6 -f Dockerfile-alpine-arm32v6 .
